const {
	generateMessage,
	generateLocationMessage,
} = require('../src/utils/messages')

let getTimeSpy

beforeAll(() => {
	// Spies on the Date class and returns a mocked Date object, with fixed value.
	const mockDate = new Date(1466424490000)
	getTimeSpy = jest.spyOn(global, 'Date').mockImplementation(() => mockDate)
})

afterAll(() => {
	// Restores all mocked objects and functions.
	getTimeSpy.mockRestore()
})

it('should generate a correct message', () => {
	const testData = {
		username: 'Dummy',
		text: 'Sample text',
	}
	const message = generateMessage(...Object.values(testData))

	expect(message).toMatchObject(testData)
	expect(message.username).toMatch(testData.username)
	expect(message.text).toMatch(testData.text)
	expect(message.createdAt).toBe(new Date().getTime())
})

it('should generate a correct location message', () => {
	const testData = {
		username: 'Dummy',
		url: 'www.google.com',
	}

	const message = generateLocationMessage(...Object.values(testData))
	expect(message).toMatchObject(testData)
	expect(message.username).toMatch(testData.username)
	expect(message.url).toMatch(testData.url)
	expect(message.createdAt).toBe(new Date().getTime())
})
