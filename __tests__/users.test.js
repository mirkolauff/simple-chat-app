const {
	users,
	addUser,
	removeUser,
	getUser,
	getUsersInRoom,
} = require('../src/utils/users')

let user1, user2, user3

beforeEach(() => {
	users.splice(0, users.length)

	user1 = {
		id: 1,
		username: 'user 1',
		room: 'room a',
	}
	user2 = {
		id: 2,
		username: 'user 2',
		room: 'room a',
	}
	user3 = {
		id: 3,
		username: 'user 3',
		room: 'room b',
	}

	users.push(user1, user2, user3)
})

it('should add a user correctly', () => {
	const user = { id: 4, username: 'user 4', room: 'room b' }
	const retValue = addUser(user)

	expect(retValue).toStrictEqual({ user })
	expect(users.length).toBe(4)
	expect(users[3]).toStrictEqual(user)
})

it('should not add an user with an already existing username', () => {
	const user = { id: 4, username: 'user 3', room: 'room b' }
	const retValue = addUser(user)

	expect(retValue).toStrictEqual({
		error: 'Username is in use!',
	})
	expect(users.length).toBe(3)
	expect(users.find((u) => u.id === user.id)).toBe(undefined)
})

it('should not add an user without a name', () => {
	const user = { id: 4, username: '', room: 'room b' }
	const retValue = addUser(user)

	expect(retValue).toStrictEqual({
		error: 'Username and room are required',
	})
	expect(users.length).toBe(3)
	expect(users.find((u) => u.id === user.id)).toBe(undefined)
})

it('should not add an user without a room', () => {
	const user = { id: 4, username: 'user 4', room: '' }
	const retValue = addUser(user)

	expect(retValue).toStrictEqual({
		error: 'Username and room are required',
	})
	expect(users.length).toBe(3)
	expect(users.find((u) => u.id === user.id)).toBe(undefined)
})

it('should remove an existing user correctly', () => {
	const user = removeUser(1)

	expect(user).toStrictEqual(user1)
	expect(users.length).toBe(2)
})

it('should not remove any users if user id has not been found', () => {
	const user = removeUser(4)
	expect(user).toBe(undefined)
	expect(users.length).toBe(3)
})

it('should return the existing user', () => {
	const user = getUser(2)
	expect(user).toStrictEqual(user2)
})

it('should should return undefined if user does not exist', () => {
	const user = getUser(4)
	expect(user).toBe(undefined)
})

it('should return all users in the same room', () => {
	const users = getUsersInRoom('room a')

	expect(users.length).toBe(2)
	expect(users[0]).toStrictEqual(user1)
	expect(users[1]).toStrictEqual(user2)
})

it('should return an empty list for an empty room', () => {
	const users = getUsersInRoom('room c')

	expect(users.length).toBe(0)
})
